// comments

// commenst are section of the code that is not red by the language

// js 2 types of comment
//1. single line comment. denoted by //
// 2. multi line comment. denoted by slah and an asterisk

//alert("Hello World");

//syntax and statements

//syntax contains the rules needed to creare a statment un a programming language
//statements are instruction we tell the application to perform
//js usually statement ends with semicolon -->


//synytax is a set of rules that describes how the statements must be constructed -->

//varibles and constants are used to contain data for various variables is names of loacation of value that can change. 

//constant is type of values that does not change once assigned. 

//variables contains value that can change as program runs

//to create a variables we use the  "let" keyword
//constant = we use the "const" keyword


let productName = "Desktop computer"
//"Destop computer " is a string
let productPrice = 18999;
//1899  is a number no need to indicate if they are numbers
const PI = 3.1416;
//PI is const bc it never change...

// rules is naming variable names start with lowercase letter for multiple words we use camelCase
// variable names should be descriptive of what values it can contain

// console - is part of a browser wherein outputs can be displayed.. it can be accessed via the console tab in any browser.

//tou output a value in the console, we use console.log function

console.log(productName); // console is an object in JS and .log is a function that allows writing of the output
console.log(productPrice);
console.log(PI);

console.log("Hello World");
console.log(12345);
console.log("I am selling a " + productName);

//  6 types of datatypes

// 1.undefined, boolen, number,string,bigint,object,symbol,null,
// object- combination of data with key values
// intentionally left to have null value

//string
let fullName = "Brandon B. Brandon";
let schoolName = "Zuitt Coding Bootcamp";
let userName = "brandon00";
//numbers
let age = 28
let numberOfPets = 5;
let desiredGrade = 98.5;

//boolean
let isSingle= true;
let hasEaten = false;

//undefined
let petName; //we did not define any value

//null
let grandChildName= null;

let person = {
    firstName: "Jobert",
    lastName: "Boyd",
    age:15,
    petNmae:"whitey"
};

//operators-allows us to perform operations or evaluate results

// assignment, arithmethc,comparison, relational,logical..

//assignment the = symbol. it assigns a value to a variable

let num1=28;
let num2=75;

//arthemitic

let sum = num1+num2;
console.log(sum);
let difference = num1 - num2;
console.log(difference);
let product = num1 * num2;
console.log(product);
let quotient = num1 / num2;
console.log(quotient);
let remainder = num2 % num1;
console.log(remainder);



let num3 = 17;
num3 = num3 - 4;//this is 
//num3-=4; is a shorthand version...

//special arithmetic  ++ --

let num4=5;
num4++;
console.log(num4);

// difference between num++ and ++num??????????????


//comparison compares 2 values if they are equal or not
// ==, === ,!=. !==

let numA=65;
let numB=65;
console.log(numA == numB);

let s1="true";
let s2=true;
console.log(s1 == s2);
console.log(s1 === s2);


//relational comapres 2 number if they are equal or not > < >= <=

let numC=25;
let numD =45;

console.log(numC > numD);
console.log(numC < numD);
console.log(numC >= numD);

//logical is compares 2 booean values
 //  (AND &&) OR || NOT !

let isTall =false;
let isDark = true;
let isHandsome=true;
let passed = isTall && isDark; // both condition shoul be true
console.log(passed);

let didPassedLowerStandard = isTall || isDark; //true
console.log(didPassedLowerStandard);// atleast one is true

console.log(!isTall); //true = reverse the value ..

let result = isTall || (isDark && isHandsome);
console.log(result);

//2 terms of function



function createFullName(fName, mName, lname){
    return fName + mName + lname;

}

let fullName1 = createFullName("Brandon","Ray", "Smith");
console.log(fullName1);

let fullName2 = createFullName("John","Robert", "Smith");
console.log(fullName2);


function addTwo( num1,num2){
        return 2*(num1 + num2)};

console.log(addTwo(54,106));


